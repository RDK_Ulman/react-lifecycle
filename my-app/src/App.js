import logo from './logo.svg';
import './App.css';
import React from 'react';
// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.js</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React, Daddy!
//         </a>
//       </header>
//     </div>
//   );
// }

// Переделал функциональный компонент в классовый 
class App extends React.Component {
  constructor(props) {
    super(props);
    
    this.state = {
      count: 0,
    }
  }

  // при первом рендеренге сначала отработает componentDidMount, потом componentWillUnmount, а потом снова componentDidMount
  // как я понимаю, это происходит из-за режима <React.StrictMode>, без него при первом рендере отрабатывает только componentDidMount и все.
  componentDidMount() {
    console.log('Оп, смонтировался компонент!')
  }

  componentDidUpdate() {
    console.log('Оп, обновился компонент!')
  }

  componentWillUnmount() {
    console.log('Оп, размонтировался компонент!')
  }

  handleClick = () => {
    const {count} = this.state;
    this.setState({count: count + 1}); //обновляю счетчик кликов по кнопке
  }

  render() {
    // task-7
    const itemString = 'Is it a string?';
    const itemNumber = 19980325;
    const itemBoolean = false;
    const itemNull = null;

    // task-8
    const arrayStrings = ['"Baller"', 'is', 'cool', 'song', 'from', 'NEFFEX'];

    // генератор случайных чисел для ключей элементов списка. Не лучшая идея, но лучше использования index в качестве ключей :D
    const random = (max, min) => {
      return Math.random() * (max - min) + min;
    };

    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.js</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React, Bro!
          </a>

          {/* пункт дз №7 */}
          <ul className='list-reset list'>
            <li>{itemString}</li>
            <li>{itemNumber}</li>

            {/* ниже в строках ничего не будет выведено. Для вывода boolean/null нужно использовать преобразование, н-р, String(item). Иначе реакт не поймет и ничего не выведет */}
            {/* <li>{itemBoolean}</li> */}
            {/* <li>{itemNull}</li> */}
            
            <li>{String(itemBoolean)}</li>
            <li>{String(itemNull)}</li>
          </ul>

          {/* пункт дз №8 */}
          <ul className='list-reset list'>
            {arrayStrings.map(item => 
              <li key={random(5, 75) + random(12, 152)}>{item}</li>
              )}
          </ul>

          {/* пункт дз №9 */}
          <button className='btn-reset btn' onClick={this.handleClick}>Wow! You clicked me: {this.state.count} times!</button>
        </header>
      </div>
    )
  }
}


export default App;
